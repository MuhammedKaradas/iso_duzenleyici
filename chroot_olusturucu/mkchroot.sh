#!/bin/bash
cd ..
if [ -f ./config/arch ]
then
  export arch=$(cat ./config/arch)
else
  export arch="amd64"
fi
if [ -f ./config/dist ]
then
  export dist=$(cat ./config/dist)
else
  export dist="stable"
fi
if [ -f ./config/pool ]
then
  export pool=$(cat ./config/pool)
else
  export pool=""
fi

debootstrap  --no-check-gpg  --arch=$arch  $dist  "$(cat ./config/chroot)"  $pool
chroot "$(cat ./config/chroot)" apt-get update
if [ "$(cat ./config/distro)" == "debian" ]
then
  chroot "$(cat ./config/chroot)" apt-get install live-config live-boot
elif [ "$(cat ./config/distro)" == "ubuntu" ]
then
  chroot "$(cat ./config/chroot)" apt-get install casper
fi
