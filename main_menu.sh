#!/bin/bash
export depo=""
export lang="$(cat config/lang)"
if [ -f "config/lang" ]
then
  export lang="$(cat config/lang)"
else
  export lang="en_us"
fi
export konum=$(pwd)
if [ "$UID" == "0" ]
then
  cat malzeme/$lang/menu
  read -n 1 secenek
  if [ "$secenek" == "1" ]
  then
    clear
    cd iso_cikartici
    chmod +x ./cikart.sh
    ./cikart.sh
  fi

  if [ "$secenek" == "2" ]
  then
    clear
    cd squashfs_yapici
    chmod +x ./make.sh
    ./make.sh
  fi

  if [ "$secenek" == "3" ]
  then
    clear
    cd iso_olusturucu
    chmod +x ./binary.sh
    ./binary.sh
  fi

  if [ "$secenek" == "4" ]
  then
    clear
    echo "$(cat malzeme/$lang/5)"
    cd chroot_olusturucu
    chmod +x mkchroot.sh
    ./mkchroot.sh
  fi

  if [ "$secenek" == "5" ]
  then
    clear
    cat $(cat malzeme/$lang/howto)
  fi

  if [ "$secenek" == "6" ]
  then
    clear
    exit
  fi

  if [ "$secenek" == "7" ]
  then
    clear
    echo "$(cat malzeme/$lang/11)[tr/en-us]"
    read langx
    if [ "$langx" == "" ]
    then
      export langx="en_us"
    fi
    if [ -f "malzeme/$langx/menu" ]
    then
      echo "$langx" > ./config/lang
    else
      echo "$(cat malzeme/$lang/12)"
      exit 1
    fi
    echo "$(cat malzeme/$lang/1)"
    read chrootx
    echo "$(cat malzeme/$lang/2)"
    read binaryx
    if [ "$chrootx" == "" ]
    then
      echo "$(pwd)/chroot" > ./config/chroot
    else
      if [ -d "malzeme/$chrootx" ]
      then
        echo "$chrootx" > ./config/chroot
      else
        echo "$(cat malzeme/$lang/12)"
        exit 1
      fi
    fi
    if [ "$binaryx" == "" ]
    then
      echo "$(pwd)/iso_olusturucu/binary/" > ./config/binary
      else
    if [ -d "malzeme/$binaryx" ]
      then
        echo "$chrootx" > ./config/binary
      else
        echo "$(cat malzeme/$lang/12)"
        exit 1
      fi
    fi
    echo "$(cat malzeme/$lang/13)[ubuntu/debian]"
    read distro
    if [ "$distro" == "" ]
    then
      export distro="ubuntu"
    fi
    if [ "$distro" == "debian" ]
    then
      sed -i "s/boot=.* components/boot=live components/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/boot=.* components/boot=live components/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/live\/vmlinuz/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/initrd.*initrd/initrd \/live\/initrd/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/live\/vmlinuz/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/initrd.*initrd/initrd \/live\/initrd/" "$(cat config/binary)"/boot/grub/grub.cfg
      echo "$distro" > ./config/distro
    elif [ "$distro" == "ubuntu" ]
    then
      sed -i "s/boot=.* components/boot=casper components/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/boot=.* components/boot=casper components/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/casper\/vmlinuz/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/initrd.*initrd/initrd \/casper\/initrd/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/casper\/vmlinuz/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/initrd.*initrd/initrd \/casper\/initrd/" "$(cat config/binary)"/boot/grub/grub.cfg
      echo "$distro" > ./config/distro
    else
      echo "$(cat malzeme/$lang/12)"
      exit 1
    fi
  fi

  if [ "$secenek" == "8" ]
  then
    clear
    echo "$(cat malzeme/$lang/8)"
    read pool
    echo "$(cat malzeme/$lang/9)"
    read dist
    echo "$(cat malzeme/$lang/10)"
    read arch
    echo "$arch" > ./config/arch
    echo "$pool" > ./config/pool
    echo "$dist" > ./config/dist
  fi

  if [ "$secenek" == "9" ]
  then
    clear
    cd efi_saglayici
    chmod +x ./efi.sh
    ./efi.sh
    cd ..
  fi
  if [ "$secenek" == "0" ]
  then
    clear
    rm -rf config/*
      sed -i "s/boot=.* components/boot=casper components/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/boot=.* components/boot=casper components/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/casper\/vmlinuz/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/initrd.*initrd/initrd \/casper\/initrd/" "$(cat config/binary)"/isolinux/live.cfg
      sed -i "s/linux.*vmlinuz/linux \/casper\/vmlinuz/" "$(cat config/binary)"/boot/grub/grub.cfg
      sed -i "s/initrd.*initrd/initrd \/casper\/initrd/" "$(cat config/binary)"/boot/grub/grub.cfg
    echo "ubuntu" > config/distro
  fi
  cd $konum
  echo "$(cat malzeme/$lang/3)"
  read  -n 1 duraklama
  clear
else
  echo "$(cat malzeme/$lang/4)"
  exit
fi
