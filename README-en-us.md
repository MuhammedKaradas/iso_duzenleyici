how to use?

Consists of 4 subprograms.

1- ISO extractor:
This program extract Your ISO file.
Allows you to modify chroot.

2- squashfs maker:
This program is the chroot you created from scratch
XZ compression and block size will compress to 512 kb. 
Later you can add this squashfs file to your live iso.

3- iso Builder:
Using the ready-made templates that come with this program,
Creates a live iso. You can do Write and install this live DVD/USB.

4- squashfs Builder
This program is used to build chroot through the debootstrap. You can
add this chroot to the live iso. You can also open and edit it with the iso extractor.

#You need:

-debootstrap

-squashfs tools

-xorriso

-mtools
