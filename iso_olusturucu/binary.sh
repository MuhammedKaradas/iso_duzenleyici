export pwd=$(pwd)
cd ..
if [ -f ./config/binary ]
then
  export binary=$(cat ./config/binary)
else
  export binary=$pwd/binary
fi

if [ -f "./config/chroot" ]
then
  export chrootx="$(cat ./config/chroot)"
  if [ -f "$chrootx/vmlinuz" ] && [ -f "$chrootx/initrd.img" ] && [ -f "./squashfs_yapici/filesystem.squashfs" ]
  then
    if [ "$(cat ./config/distro)" == "ubuntu" ]
    then
      export subdir=casper
    elif [ "$(cat ./config/distro)" == "debian" ]
    then
      export subdir=live
    else
      exit 1
    fi
    rm -rf $binary/$subdir 2> /dev/null
    mkdir $binary/$subdir 2> /dev/null
    dd if="$chrootx/vmlinuz" of=$binary/$subdir/vmlinuz
    dd if="$chrootx/initrd.img" of=$binary/$subdir/initrd.img
    mv ./squashfs_yapici/filesystem.squashfs $binary/$subdir/filesystem.squashfs
  fi
fi
xorriso -as mkisofs \
        -iso-level 3 -rock -joliet \
        -max-iso9660-filenames -omit-period \
        -omit-version-number -relaxed-filenames -allow-lowercase \
        -volid "CustomLiveIso" \
        -eltorito-boot isolinux/isolinux.bin \
        -eltorito-catalog isolinux/isolinux.cat \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -eltorito-alt-boot -e boot/grub/efi.img -isohybrid-gpt-basdat -no-emul-boot \
        -isohybrid-mbr $binary/isolinux/isohybrid-mbr \
-output "live-image-$(date +%y%m%d%H%M%S).hybrid.iso" $binary
